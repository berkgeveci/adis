//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include <adis/DataSetReader.h>

#ifdef USE_MPI
#include <mpi.h>
#endif
#include <adios2.h>

#include <string>
#include <unordered_map>
#include <vector>
#include <sstream>

#include <vtkm/cont/Algorithm.h>
#include <vtkm/cont/CoordinateSystem.hxx>
#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/worklet/ScatterPermutation.h>

static const int num_steps = 10;
static const int size = 5;

void analysis(adis::io::DataSetReader& reader, int min, int max)
{
  std::unordered_map<std::string, std::string> paths;
  paths["the_source"] = "DataReader";

  auto metaData = reader.ReadMetaData(paths);
  using FieldInfoType =
    adis::metadata::Vector<adis::metadata::FieldInformation>;
  auto& fields = metaData.Get<FieldInfoType>(adis::keys::FIELDS());
  if (fields.Data.size() != 1)
  {
    std::stringstream ss;
    ss << "Error: expected 1 field, got " << fields.Data.size();
    throw std::runtime_error(ss.str());
  }

  adis::metadata::MetaData selections;
  adis::metadata::Vector<size_t> blockSelection;
  blockSelection.Data.push_back(0);
  selections.Set(adis::keys::BLOCK_SELECTION(), blockSelection);

  FieldInfoType fieldSelection;
  fieldSelection.Data.push_back(
    adis::metadata::FieldInformation(
      "testvar", vtkm::cont::Field::Association::POINTS));
  selections.Set(adis::keys::FIELDS(), fieldSelection);

  vtkm::cont::PartitionedDataSet output = reader.ReadDataSetNextStep(paths, selections);
  if (output.GetNumberOfPartitions() != 1)
  {
    std::stringstream ss;
    ss << "Error: expected 1 block, got " << output.GetNumberOfPartitions();
    throw std::runtime_error(ss.str());
  }

  vtkm::cont::DataSet ds = output.GetPartition(0);
  vtkm::IdComponent nFields = ds.GetNumberOfFields();
  if (nFields != 1)
  {
    std::stringstream ss;
    ss << "Error: expected 1 output array, got " << nFields;
    throw std::runtime_error(ss.str());
  }

  if (!ds.HasField("testvar", vtkm::cont::Field::Association::POINTS))
  {
    throw std::runtime_error("Error: expected testvar array, but not found");
  }

  const auto& arrHandle
    = ds.GetField("testvar").GetData().Cast<vtkm::cont::ArrayHandle<int> >();
  vtkm::cont::ArrayHandle <vtkm::Range> rangeArray =
    vtkm::cont::ArrayRangeCompute(arrHandle);
  auto rangePortal = rangeArray.GetPortalConstControl();
  if (rangePortal.Get(0).Min != min)
  {
    std::stringstream ss;
    ss << "Error: expected min = " << min << ", but got " << rangePortal.Get(0).Min;
    throw std::runtime_error(ss.str());
  }

  if (rangePortal.Get(0).Max != max)
  {
    std::stringstream ss;
    ss << "Error: expected max = " << max << ", but got " << rangePortal.Get(0).Max;
    throw std::runtime_error(ss.str());
  }
}

int main(int argc, char** argv)
{
#ifdef USE_MPI
  MPI_Init(&argc, &argv);
  adios2::ADIOS adios(MPI_COMM_WORLD, adios2::DebugON);
#else
  adios2::ADIOS adios(adios2::DebugON);
#endif

  const std::string writer_id = "output.bp";
  const std::string source_name = "the_source";

  // First set up ADIOS and ADIS
  adios2::IO io = adios.DeclareIO("DataWriter");
  //params["verbose"] = "5"; // optional; sets verbose in ADIOS
  adis::io::DataSetReader reader(argv[1]);
  adis::DataSourceParams params;
  params["engine_type"] = "Inline";
  params["writer_id"] = writer_id;
  reader.SetDataSourceParameters(source_name, std::move(params));
  reader.SetDataSourceIO(source_name, &io);

  adios2::Variable<int> testVar = io.DefineVariable<int>("testvar",
      {size, size, size}, {0, 0, 0}, {size, size, size});

  // in the application if you call io.Open before setting up ADIS,
  // you'll have to call io.SetEngine("Inline") as well.
  // If ADIS is set up before the io.Open() call, ADIS handles it.
  adios2::Engine writer = io.Open(writer_id, adios2::Mode::Write);

  std::vector<int> data(size * size * size, 0);
  int min = INT_MAX, max = 0;
  for (int ii = 0; ii < num_steps; ++ii)
  {
    for (int idx = 0; idx < data.size(); ++idx)
    {
      data[idx] += idx;
      if (data[idx] < min)
        min = data[idx];
      if (data[idx] > max)
        max = data[idx];
    }

    writer.BeginStep();
    writer.Put<int>(testVar, data.data());
    writer.EndStep();

    analysis(reader, min, max);
  }

  writer.Close();
#ifdef USE_MPI
  MPI_Finalize();
#endif
  return 0;
}
