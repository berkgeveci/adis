## TODO

- Write a README.md which gives a cogent explanation of the purpose of this library.
- Do not allow empty `.json` files to pass the schema validator.
- Incorporate the schema validation work done by Zhe Wang.
- Support unstructured grids.
- Allow loading both files and strings in `DataSetReader`.
- Remove `test/vtk-uns-grid.json`, as it does not pass schema validation.
- Add CI
