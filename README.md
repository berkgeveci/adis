
## ADIS: Adaptable Data Interfaces and Services

ADIS is a library for adding metadata to ADIOS2 files so they can be more easily read and understood by the VTK.

The metadata required to visualize a dataset is often different than the metadata required in other contexts, so despite the fact we want our simulation data to be "self-describing", this statement has different meanings in different contexts, and hence it is almost impossible to realize in practice.

To use ADIS, you must first create a `.json` file which has information relevant for processing the file in VTK.

## Dependencies

ADIS depends of VTK-m and ADIOS2.

A commit of VTK-m which is known to work is 30733609e.

A commit of ADIOS2 which is known to work is e36391e354, but we have never found a commit that doesn't work, so this is not a huge worry.
Run the unit tests if doubt arises.


## Testing

`ADIS` uses `git lfs` to manage its datasets.
This must be initialized after cloning.

```
adis$ git lfs install
adis$ git lfs pull
```

The tests are managed by `ctest`, so in the build directory just run

```
build_adis$ ctest -V
```
