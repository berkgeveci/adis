cmake_minimum_required(VERSION 3.9)
project(ADIS LANGUAGES C CXX VERSION 0.1)

list(INSERT CMAKE_MODULE_PATH 0 "${PROJECT_SOURCE_DIR}/cmake")

find_package(RapidJSON REQUIRED)
find_package(ADIOS2 REQUIRED)
find_package(VTKm REQUIRED)

option(BUILD_SHARED_LIBS "Build adis with shared libraries" ON)
set(ADIS_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})
option(ADIS_ENABLE_TESTING "Enable ADIS Testing" ON)

add_subdirectory(adis)

if (ADIS_ENABLE_TESTING)
  enable_testing()
  include(CTest)
  add_subdirectory(tests)
endif()
