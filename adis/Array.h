//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_datamodel_Array_H_
#define adis_datamodel_Array_H_

#include <vtkm/cont/VariantArrayHandle.h>

#include <adis/DataModel.h>
#include <adis/Value.h>

namespace adis
{
namespace datamodel
{

/// \brief Superclass for all specialized array implementations.
struct ArrayBase : public DataModelBase
{
  /// Reads and returns array handles. Has to be implemented
  /// by subclasses.
  virtual std::vector<vtkm::cont::VariantArrayHandle> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections) = 0;

  /// Returns the number of blocks in the underlying variable.
  /// Used by the reader to provide meta-data on blocks.
  /// Has to be implemented by subclasses.
  virtual size_t GetNumberOfBlocks(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources) = 0;

    virtual ~ArrayBase() {};
};

/// \brief Data model object for VTK-m array handles.
///
/// \c adis::datamodel::Array is responsible of creating
/// VTK-m \c ArrayHandles by loading data defined by the ADIS
/// data model. This class delegates its responsibilities to
/// one of the specialized \c ArrayBase subclasses it creates
/// during JSON parsing.
struct Array : public DataModelBase
{
  /// Overridden to handle Array specific items.
  /// This will create an internal ArrayBase subclass
  /// depending on the array_type value.
  void ProcessJSON(const rapidjson::Value& json,
                   DataSourcesType& sources) override;

  /// Reads and returns array handles. Handled by the
  /// internal ArrayBase subclass.
  std::vector<vtkm::cont::VariantArrayHandle> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections);

  /// Returns the number of blocks in the underlying variable.
  /// Used by the reader to provide meta-data on blocks.
  /// Handled by the internal ArrayBase subclass.
  size_t GetNumberOfBlocks(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources);

private:
  std::unique_ptr<ArrayBase> ArrayImpl = nullptr;
};


/// \brief Class to read \c ArrayHandle objects.
///
/// \c ArrayBasic reads ArrayHandle objects with basic storage.
struct ArrayBasic : public ArrayBase
{
  void ProcessJSON(const rapidjson::Value& json,
                   DataSourcesType& sources) override;

  /// Reads and returns array handles. The heavy-lifting is
  /// handled by the \c DataModelBase \c ReadSelf() method.
  /// The paths are passed to the \c DataSources to create
  /// file paths. \c selections restrict the data that is loaded.
  std::vector<vtkm::cont::VariantArrayHandle> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections) override;

  /// Returns the number of blocks in the underlying variable.
  /// Used by the reader to provide meta-data on blocks.
  size_t GetNumberOfBlocks(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources) override;

private:
  adis::io::IsVector IsVector = adis::io::IsVector::Auto;
};

/// \brief Class to read \c ArrayHandleUniformPointCoordinates objects.
///
/// \c ArrayUniformPointCoordinates creates ArrayHandleUniformPointCoordinates
/// objects. It depends on a number of Value objects to obtains dimensions
/// (per block), origin and spacing. The origin of each block is computed based
/// on spacing and additional values provided by the Dimensions
/// Value object (start indices for each block).
struct ArrayUniformPointCoordinates : public ArrayBase
{
  /// Overridden to handle ArrayUniformPointCoordinates specific items.
  void ProcessJSON(const rapidjson::Value& json,
                   DataSourcesType& sources) override;

  /// Reads and returns array handles. This class depends on
  /// a number of Value objects to obtains dimensions (per block),
  /// origin and spacing. The origin of each block is computed based
  /// on spacing and additional values provided by the Dimensions
  /// Value object (start indices for each block).
  std::vector<vtkm::cont::VariantArrayHandle> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections) override;

  /// Returns the number of blocks in the underlying variable.
  /// Used by the reader to provide meta-data on blocks.
  size_t GetNumberOfBlocks(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources) override;

private:
  std::unique_ptr<Value> Dimensions = nullptr;
  std::unique_ptr<Value> Origin = nullptr;
  std::unique_ptr<Value> Spacing = nullptr;
};

struct ArrayCartesianProduct : public ArrayBase
{
  /// Overridden to handle ArrayCartesianProduct specific items.
  void ProcessJSON(const rapidjson::Value& json,
                   DataSourcesType& sources) override;

  /// Reads and returns array handles. This class depends on
  /// three separate (basic) array  objects that form the
  /// cartesian product.
  std::vector<vtkm::cont::VariantArrayHandle> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections) override;

  /// Returns the number of blocks in the underlying variable.
  /// Used by the reader to provide meta-data on blocks.
  /// Uses the number of blocks in the first (x) array.
  size_t GetNumberOfBlocks(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources) override;

private:
  std::unique_ptr<Array> XArray = nullptr;
  std::unique_ptr<Array> YArray = nullptr;
  std::unique_ptr<Array> ZArray = nullptr;
};


}
}

#endif
