//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef adis_ADISTypes_H_
#define adis_ADISTypes_H_

#include <unordered_map>

namespace adis
{

/// Parameters for an individual data source, e.g., Parameters needed
/// by ADIOS for configuring an Engine.
using DataSourceParams = std::unordered_map<std::string, std::string>;

/// Parameters for all data sources mapped to their source name.
/// The key must match the name given for the data source in the JSON file.
using Params = std::unordered_map<std::string, DataSourceParams>;

}

#endif
