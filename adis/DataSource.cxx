//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include <adis/DataSource.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleGroupVec.h>
#include <vtkm/cont/Storage.h>

#include <algorithm>
#include <numeric>

#include <iostream>

#ifdef ADIOS2_HAVE_MPI
#include <mpi.h>
#endif

namespace adis
{
namespace io
{

void DataSource::SetDataSourceParameters(const DataSourceParams& params)
{
  this->SourceParams = params;
}

void DataSource::SetDataSourceIO(void* io)
{
  if (!io)
  {
    return;
  }
  this->AdiosIO = *(reinterpret_cast<adios2::IO*>(io));
  this->SetupEngine();
}

void DataSource::SetupEngine()
{
  auto it = this->SourceParams.find("engine_type");
  std::string engine = "BPFile";
  if (it != this->SourceParams.end())
  {
    engine = it->second;
  }

  if (engine == "BPFile")
  {
    this->AdiosEngineType = ADIOSEngineType::BPFile;
    this->AdiosIO.SetEngine("BPFile");
  }
  else if (engine == "SST")
  {
    this->AdiosEngineType = ADIOSEngineType::SST;
    this->AdiosIO.SetEngine("SST");
  }
  else if (engine == "Inline")
  {
    this->AdiosEngineType = ADIOSEngineType::Inline;
    if (!this->AdiosIO)
    {
      throw std::runtime_error("Inline engine requires passing (to DataSetReader) "
          "a valid pointer to an adios2::IO object.");
    }
    this->AdiosIO.SetEngine("Inline");

    it = this->SourceParams.find("writer_id");
    if (it == this->SourceParams.end())
    {
      throw std::runtime_error("Inline engine requires a valid writer_id.");
    }
    this->AdiosIO.SetParameter("writerID", it->second);
  }
  else
  {
    throw std::runtime_error("parameter engine_type must be BPFile, SST or Inline.");
  }

  it = this->SourceParams.find("verbose");
  if (it != this->SourceParams.end())
  {
    this->AdiosIO.SetParameter("verbose", it->second);
  }
}

void DataSource::OpenSource(const std::string& fname)
{
  //if the reader (ADIOS engine) is already been set, do nothing
  if (this->Reader)
  {
    return;
  }

  if (!this->AdiosIO)
  {
    if (!this->Adios)
    {
      //if the factory pointer and the specific IO is empty
      //reset the implementation
#ifdef ADIOS2_HAVE_MPI
      this->Adios.reset(
        new adios2::ADIOS(MPI_COMM_WORLD, adios2::DebugON));
#else
      this->Adios.reset(new adios2::ADIOS(adios2::DebugON));
#endif
    }
    //if the factory is not empty, generate the io used by adis internally
    this->AdiosIO = this->Adios->DeclareIO("adios-io-read");
    this->SetupEngine();
  }

  this->Reader = this->AdiosIO.Open(fname, adios2::Mode::Read);
  this->Refresh();
}

void DataSource::Refresh()
{
  this->AvailVars =  this->AdiosIO.AvailableVariables();
  this->AvailAtts =  this->AdiosIO.AvailableAttributes();
}

template <typename VariableType, typename VecType>
vtkm::cont::VariantArrayHandle AllocateArrayHandle(
  size_t bufSize, VariableType*& buffer)
{
  vtkm::cont::internal::Storage<
    VecType, vtkm::cont::StorageTagBasic> storage;
  storage.Allocate(bufSize);
  buffer = reinterpret_cast<VariableType*>(storage.GetArray());
  return vtkm::cont::ArrayHandle<VecType>(std::move(storage));
}

template <typename VariableType, vtkm::IdComponent Dim>
vtkm::cont::VariantArrayHandle AllocateArrayHandle(
  const VariableType* vecData, size_t bufSize)
{
  vtkm::cont::ArrayHandle<VariableType> arrayHandle =
    vtkm::cont::make_ArrayHandle(vecData, bufSize);
  return vtkm::cont::make_ArrayHandleGroupVec<Dim>(arrayHandle);
}

template <typename VariableType>
vtkm::cont::VariantArrayHandle ReadVariableInternal(
  adios2::Engine& reader,
  adios2::Variable<VariableType>& varADIOS2,
  size_t blockId,
  ADIOSEngineType engineType,
  IsVector isit=IsVector::Auto)
{
  auto blocksInfo =  reader.BlocksInfo(varADIOS2, reader.CurrentStep());
  const auto& shape = blocksInfo[blockId].Count;
  size_t bufSize = 1;
  for(auto n : shape)
  {
    bufSize *= n;
  }

  vtkm::cont::VariantArrayHandle retVal;
  VariableType* buffer;

  if (engineType == ADIOSEngineType::Inline)
  {
    // For the inline engine we can grab the pointer to the data
    // instead of data being copied into a buffer.
    // And this can be handled the same way whether it's a
    // vector or not
    varADIOS2.SetBlockSelection(blockId);
    reader.Get(varADIOS2, blocksInfo[blockId]);
    reader.PerformGets();
  }

  // This logic is used to determine if a variable is a
  // vector (in which case we need to read it as 2D) or
  // not (in which case we need to read it as 1D even when
  // it is a multi-dimensional variable because VTK-m expects
  // it as such)
  bool isVector;
  if (isit == IsVector::Auto)
  {
    // If we are in auto mode, assume all 2D variables are
    // vectors. This is the default.
    isVector = shape.size() == 2;
  }
  else
  {
    // Otherwise, use what the data model says.
    isVector = (isit == IsVector::Yes) ? true : false;
  }

  if (!isVector)
  {
    if (engineType == ADIOSEngineType::Inline)
    {
      const VariableType* vecData = blocksInfo[blockId].Data();
      vtkm::cont::ArrayHandle<VariableType> arrayHandle =
        vtkm::cont::make_ArrayHandle(vecData, bufSize);
      retVal = arrayHandle;
    }
    else
    {
      vtkm::cont::internal::Storage<
        VariableType, vtkm::cont::StorageTagBasic> storage;
      storage.Allocate(bufSize);
      buffer = storage.GetArray();
      vtkm::cont::ArrayHandle<VariableType> arrayHandle(std::move(storage));
      retVal = arrayHandle;
      reader.Get(varADIOS2, buffer);
    }
  }
  else
  {
    // Vector: the last dimension is assumed to be the vector
    // components. Previous dimensions are collapsed together.
    size_t nDims = shape.size();
    if (nDims < 2)
    {
      throw std::runtime_error("1D array cannot be a vector");
    }

    size_t bufSize2 = 1;
    for(size_t i=0; i<nDims-1; i++)
    {
      bufSize2 *= shape[i];
    }
    if (engineType == ADIOSEngineType::Inline)
    {
      const VariableType* vecData = blocksInfo[blockId].Data();
      switch(shape[nDims-1])
      {
        case 1:
          retVal =
            AllocateArrayHandle<VariableType, 1>(
              vecData, bufSize);
          break;
        case 2:
          retVal =
            AllocateArrayHandle<VariableType, 2>(
              vecData, bufSize2);
          break;
        case 3:
          retVal =
            AllocateArrayHandle<VariableType, 3>(
              vecData, bufSize2);
          break;
        default:
          break;
      }
    }
    else
    {
      switch(shape[nDims-1])
      {
        case 1:
          retVal =
            AllocateArrayHandle<VariableType, VariableType>(
              bufSize, buffer);
          break;
        case 2:
          retVal =
            AllocateArrayHandle<VariableType, vtkm::Vec<VariableType, 2> >(
              bufSize2, buffer);
          break;
        case 3:
          retVal =
            AllocateArrayHandle<VariableType, vtkm::Vec<VariableType, 3> >(
              bufSize2, buffer);
          break;
        default:
          break;
      }
    }
    reader.Get(varADIOS2, buffer);
  }

  return retVal;
}

template <typename VariableType>
size_t GetNumberOfBlocksInternal(
  adios2::IO& adiosIO,
  adios2::Engine& reader,
  const std::string& varName)
{
  auto varADIOS2 =
      adiosIO.InquireVariable<VariableType>(varName);
  auto blocksInfo =  reader.BlocksInfo(varADIOS2, reader.CurrentStep());
  return blocksInfo.size();
}


template <typename VariableType>
std::vector<vtkm::cont::VariantArrayHandle> ReadVariableBlocksInternal(
  adios2::IO& adiosIO,
  adios2::Engine& reader,
  const std::string& varName,
  const adis::metadata::MetaData& selections,
  ADIOSEngineType engineType,
  IsVector isit=IsVector::Auto)
{
  std::vector<vtkm::cont::VariantArrayHandle> arrays;
  if (selections.Has(adis::keys::BLOCK_SELECTION()) &&
      selections.Get<adis::metadata::Vector<size_t> >(
        adis::keys::BLOCK_SELECTION()).Data.empty())
  {
    return arrays;
  }
  auto varADIOS2 =
      adiosIO.InquireVariable<VariableType>(varName);

  if(!varADIOS2)
  {
    throw std::runtime_error("adiosIO.InquireVariable() failed on variable " + varName);
  }
  auto blocksInfo =  reader.BlocksInfo(varADIOS2, reader.CurrentStep());
  std::vector<size_t> blocksToReallyRead;
  if (!selections.Has(adis::keys::BLOCK_SELECTION()))
  {
    size_t nBlocks = blocksInfo.size();
    blocksToReallyRead.resize(nBlocks);
    std::iota(blocksToReallyRead.begin(),
              blocksToReallyRead.end(),
              0);
  }
  else
  {
    const std::vector<size_t>& blocksToRead =
      selections.Get<adis::metadata::Vector<size_t> >(
        adis::keys::BLOCK_SELECTION()).Data;
    blocksToReallyRead = blocksToRead;
  }
  arrays.reserve(blocksToReallyRead.size());

  if (selections.Has(adis::keys::STEP_SELECTION()) &&
      varADIOS2.Steps() > 1)
  {
    size_t stepSel = selections.Get<adis::metadata::Index>(
      adis::keys::STEP_SELECTION()).Data;
    varADIOS2.SetStepSelection({stepSel, 1});
  }
  for(auto blockId : blocksToReallyRead)
  {
    varADIOS2.SetBlockSelection(blockId);
    arrays.push_back(
      ReadVariableInternal<VariableType>(reader, varADIOS2, blockId, engineType, isit));
  }

  return arrays;
}

template <typename VariableType>
std::vector<vtkm::cont::VariantArrayHandle> GetDimensionsInternal(
  adios2::IO& adiosIO,
  adios2::Engine& reader,
  const std::string& varName,
  const adis::metadata::MetaData& selections)
{
  auto varADIOS2 =
      adiosIO.InquireVariable<VariableType>(varName);
  size_t currentStep = reader.CurrentStep();
  auto blocksInfo =  reader.BlocksInfo(varADIOS2, currentStep);
  if(blocksInfo.size() == 0)
  {
    throw std::runtime_error("blocksInfo is 0 for variable: " + varName);
  }

  std::vector<size_t> blocksToReallyRead;
  if (!selections.Has(adis::keys::BLOCK_SELECTION()) ||
      selections.Get<adis::metadata::Vector<size_t> >(
        adis::keys::BLOCK_SELECTION()).Data.empty())
  {
    size_t nBlocks = blocksInfo.size();
    blocksToReallyRead.resize(nBlocks);
    std::iota(blocksToReallyRead.begin(),
              blocksToReallyRead.end(),
              0);
  }
  else
  {
    const std::vector<size_t>& blocksToRead =
      selections.Get<adis::metadata::Vector<size_t> >(
        adis::keys::BLOCK_SELECTION()).Data;
    blocksToReallyRead = blocksToRead;
  }

  std::vector<vtkm::cont::VariantArrayHandle> arrays;
  arrays.reserve(blocksToReallyRead.size());

  for(auto blockId : blocksToReallyRead)
  {
    std::vector<size_t> shape = blocksInfo[blockId].Count;
    std::reverse(shape.begin(), shape.end());
    std::vector<size_t> start = blocksInfo[blockId].Start;
    std::reverse(start.begin(), start.end());
    shape.insert(shape.end(), start.begin(), start.end());
    arrays.push_back(vtkm::cont::make_ArrayHandle(shape, vtkm::CopyFlag::On));
  }

  return arrays;
}

#define adisTemplateMacro(call) \
  switch(type[0]) \
  { \
    case 'c': \
    { \
      using adis_TT = char; \
      return call; \
      break; \
    } \
    case 'f': \
    { \
      using adis_TT = float; \
      return call; \
      break; \
    } \
    case 'd': \
    { \
      using adis_TT = double; \
      return call; \
      break; \
    } \
    case 'i': \
      if (type == "int") \
      { \
        using adis_TT = int; \
        return call; \
      } \
      else if (type == "int8_t") \
      { \
        using adis_TT = int8_t; \
        return call; \
      } \
      else if (type == "int16_t") \
      { \
        using adis_TT = int16_t; \
        return call; \
      } \
      else if (type == "int32_t") \
      { \
        using adis_TT = int32_t; \
        return call; \
      } \
      else if (type == "int64_t") \
      { \
        using adis_TT = vtkm::Id; \
        return call; \
      } \
      break; \
    case 'l': \
      if (type == "long long int") \
      { \
        using adis_TT = vtkm::Id; \
        return call; \
      } \
      else if (type == "long int") \
      { \
        using adis_TT = long int; \
        return call; \
      } \
      break; \
    case 's': \
      if (type == "short") \
      { \
        using adis_TT = long int; \
        return call; \
      } \
      else if (type == "signed char") \
      { \
        using adis_TT = signed char; \
        return call; \
      } \
      break; \
    case 'u': \
      if (type == "unsigned char") \
      { \
        using adis_TT = unsigned char; \
        return call; \
      } \
      else if (type == "unsigned int") \
      { \
        using adis_TT = unsigned int; \
        return call; \
      } \
      else if (type == "unsigned long int") \
      { \
        using adis_TT = unsigned long int; \
        return call; \
      } \
      else if (type == "unsigned long long int") \
      { \
        using adis_TT = unsigned long long int; \
        return call; \
      } \
      else if (type == "uint8_t") \
      { \
        using adis_TT = uint8_t; \
        return call; \
      } \
      else if (type == "uint16_t") \
      { \
        using adis_TT = uint16_t; \
        return call; \
      } \
      else if (type == "uint32_t") \
      { \
        using adis_TT = uint32_t; \
        return call; \
      } \
      else if (type == "uint64_t") \
      { \
        using adis_TT = uint64_t; \
        return call; \
      } \
      break; \
  } \

std::vector<vtkm::cont::VariantArrayHandle> DataSource::GetVariableDimensions(
    const std::string& varName,
    const adis::metadata::MetaData& selections)
{
  if(!this->Reader)
  {
    throw std::runtime_error("Cannot read variable without setting the adios engine.");
  }
  auto itr = this->AvailVars.find(varName);
  if (itr == this->AvailVars.end())
  {
    throw std::runtime_error("Variable " + varName + " was not found.");
  }

  const std::string& type = itr->second["Type"];
  if(type.empty())
  {
    throw std::runtime_error("Variable type unavailable.");
  }

  adisTemplateMacro(GetDimensionsInternal<adis_TT>(
        this->AdiosIO, this->Reader, varName, selections));

  throw std::runtime_error("Unsupported variable type " + type);
}

std::vector<vtkm::cont::VariantArrayHandle>
  DataSource::ReadVariable(const std::string& varName,
                           const adis::metadata::MetaData& selections,
                           IsVector isit)
{
  if(!this->Reader)
  {
    throw std::runtime_error("Cannot read variable without setting the adios engine.");
  }
  auto itr = this->AvailVars.find(varName);
  if (itr == this->AvailVars.end())
  {
    throw std::runtime_error("Variable " + varName + " was not found.");
  }
  const std::string& type = itr->second["Type"];
  if(type.empty())
  {
    throw std::runtime_error("Variable type unavailable.");
  }

  adisTemplateMacro(ReadVariableBlocksInternal<adis_TT>(
        this->AdiosIO, this->Reader, varName, selections, this->AdiosEngineType, isit));

  throw std::runtime_error("Unsupported variable type " + type);
}

size_t DataSource::GetNumberOfBlocks(const std::string& varName)
{
  if(!this->Reader)
  {
    throw std::runtime_error("Cannot read variable without setting the adios engine.");
  }
  auto itr = this->AvailVars.find(varName);
  if (itr == this->AvailVars.end())
  {
    throw std::runtime_error("Variable " + varName + " was not found.");
  }
  const std::string& type = itr->second["Type"];

  adisTemplateMacro(GetNumberOfBlocksInternal<adis_TT>(
      this->AdiosIO, this->Reader, varName));

  throw std::runtime_error("Unsupported variable type " + type);
}

void DataSource::DoAllReads()
{
  if(!this->Reader)
  {
    throw std::runtime_error("Cannot read variables without setting the adios engine.");
  }
  this->Reader.PerformGets();
}

void DataSource::BeginStep()
{
  if(!this->Reader)
  {
    throw std::runtime_error("Cannot read variables without setting the adios engine.");
  }

  if (this->Reader.BeginStep() == adios2::StepStatus::OK)
  {
    this->Refresh();
  }
}

size_t DataSource::CurrentStep()
{
  if(!this->Reader)
  {
    throw std::runtime_error("Cannot get step without setting the adios engine.");
  }

  return this->Reader.CurrentStep();
}

void DataSource::EndStep()
{
  if(!this->Reader)
  {
    throw std::runtime_error("Cannot read variables without setting the adios engine.");
  }

  this->Reader.EndStep();
}

}
}
