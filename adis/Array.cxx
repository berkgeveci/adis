//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include <adis/Array.h>

#include <vtkm/cont/ArrayHandleUniformPointCoordinates.h>

namespace adis
{
namespace datamodel
{

std::vector<vtkm::cont::VariantArrayHandle> Array::Read(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources,
  const adis::metadata::MetaData& selections)
{
  return this->ArrayImpl->Read(paths, sources, selections);
}

size_t Array::GetNumberOfBlocks(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources)
{
  return this->ArrayImpl->GetNumberOfBlocks(paths, sources);
}

void Array::ProcessJSON(const rapidjson::Value& json,
                        DataSourcesType& sources)
{
  if (!json.HasMember("array_type") || !json["array_type"].IsString())
  {
    throw std::runtime_error(
      this->ObjectName  + " must provide a valid array_type.");
  }
  const std::string& arrayType = json["array_type"].GetString();
  if (arrayType == "basic")
  {
    this->ArrayImpl.reset(new ArrayBasic());
  }
  else if (arrayType == "uniform_point_coordinates")
  {
    this->ArrayImpl.reset(new ArrayUniformPointCoordinates());
  }
  else if (arrayType == "cartesian_product")
  {
    this->ArrayImpl.reset(new ArrayCartesianProduct());
  }
  else
  {
    throw std::runtime_error(arrayType + " is not a valid array type.");
  }
  this->ArrayImpl->ProcessJSON(json, sources);
}

void ArrayBasic::ProcessJSON(const rapidjson::Value& json,
                             DataSourcesType& sources)
{
  this->ArrayBase::ProcessJSON(json, sources);

  if (json.HasMember("is_vector"))
  {
    const std::string& isVector = json["is_vector"].GetString();
    if (isVector == "true")
    {
      this->IsVector = adis::io::IsVector::Yes;
    }
    else if (isVector == "false")
    {
      this->IsVector = adis::io::IsVector::No;
    }
    else if (isVector == "auto")
    {
      this->IsVector = adis::io::IsVector::Auto;
    }
    else
    {
      throw std::runtime_error(
        "Unrecognized value for is_vector: " + isVector);
    }
  }
}

std::vector<vtkm::cont::VariantArrayHandle> ArrayBasic::Read(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources,
  const adis::metadata::MetaData& selections)
{
  return this->ReadSelf(paths, sources, selections, this->IsVector);
}

size_t ArrayBasic::GetNumberOfBlocks(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources)
{
  auto itr = paths.find(this->DataSourceName);
  if (itr == paths.end())
  {
    throw std::runtime_error("Could not find data_source with name "
      + this->DataSourceName + " among the input paths.");
  }
  const auto& ds = sources[this->DataSourceName];
  std::string path = itr->second + ds->FileName;
  ds->OpenSource(path);
  return ds->GetNumberOfBlocks(this->VariableName);
}

void ArrayUniformPointCoordinates::ProcessJSON(const rapidjson::Value& json,
                                               DataSourcesType& sources)
{
  if (!json.HasMember("dimensions") || !json["dimensions"].IsObject())
  {
    throw std::runtime_error(
      this->ObjectName  + " must provide a dimensions object.");
  }
  this->Dimensions.reset(new Value());
  const auto& dimensions = json["dimensions"];
  this->Dimensions->ProcessJSON(dimensions, sources);

  if (json.HasMember("origin") && json["origin"].IsObject())
  {
    this->Origin.reset(new Value());
    const auto& origin = json["origin"];
    this->Origin->ProcessJSON(origin, sources);
  }

  if (json.HasMember("spacing") && json["spacing"].IsObject())
  {
    this->Spacing.reset(new Value());
    const auto& spacing = json["spacing"];
    this->Spacing->ProcessJSON(spacing, sources);
  }
}

std::vector<vtkm::cont::VariantArrayHandle>
  ArrayUniformPointCoordinates::Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections)
{
  std::vector<vtkm::cont::VariantArrayHandle> dims =
    this->Dimensions->Read(paths, sources, selections);
  std::vector<vtkm::cont::VariantArrayHandle> origins;
  if (this->Origin)
  {
    origins = this->Origin->Read(paths, sources, selections);
  }
  std::vector<vtkm::cont::VariantArrayHandle> spacings;
  if (this->Spacing)
  {
    spacings = this->Spacing->Read(paths, sources, selections);
  }
  std::vector<vtkm::cont::VariantArrayHandle> ret;
  size_t nDims = dims.size();
  ret.reserve(nDims);
  for(const auto& array : dims)
  {
    // const auto& array = dims[i];
    auto dimsB = array.Cast<vtkm::cont::ArrayHandle<size_t> >();
    auto dimsPortal = dimsB.GetPortalConstControl();
    vtkm::Id3 dimValues(dimsPortal.Get(0),
                        dimsPortal.Get(1),
                        dimsPortal.Get(2));
    vtkm::Vec<vtkm::FloatDefault, 3> originA;
    originA = vtkm::Vec<vtkm::FloatDefault, 3>(0.0, 0.0, 0.0);
    vtkm::Vec<vtkm::FloatDefault, 3> spacingA;
    spacingA = vtkm::Vec<vtkm::FloatDefault, 3>(1.0, 1.0, 1.0);
    if (this->Origin)
    {
      const auto& origin = origins[0];
      auto originB = origin.Cast<vtkm::cont::ArrayHandle<double> >();
      auto originPortal = originB.GetPortalConstControl();
      originA = vtkm::Vec<vtkm::FloatDefault, 3>(originPortal.Get(0),
                                                 originPortal.Get(1),
                                                 originPortal.Get(2));
    }
    if (this->Spacing)
    {
      const auto& spacing = spacings[0];
      auto spacingB = spacing.Cast<vtkm::cont::ArrayHandle<double> >();
      auto spacingPortal = spacingB.GetPortalConstControl();
      spacingA = vtkm::Vec<vtkm::FloatDefault, 3>(spacingPortal.Get(0),
                                                  spacingPortal.Get(1),
                                                  spacingPortal.Get(2));
    }
    // Shift origin to a local value. We have to do this because
    // VTK-m works with dimensions rather than extents and therefore
    // needs local origin.
    for(size_t i=0; i<3; i++)
    {
      originA[i] = originA[i] + spacingA[i] * dimsPortal.Get(i+3);
    }
    vtkm::cont::ArrayHandleUniformPointCoordinates ah(
      dimValues, originA, spacingA);
    ret.push_back(ah);
  }
  return ret;
}

size_t ArrayUniformPointCoordinates::GetNumberOfBlocks(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources)
{
  return this->Dimensions->GetNumberOfBlocks(paths, sources);
}

void ArrayCartesianProduct::ProcessJSON(const rapidjson::Value& json,
                                        DataSourcesType& sources)
{
  if (!json.HasMember("x_array") || !json["x_array"].IsObject())
  {
    throw std::runtime_error(
      this->ObjectName  + " must provide a x_array object.");
  }
  this->XArray.reset(new Array());
  const auto& xarray = json["x_array"];
  this->XArray->ProcessJSON(xarray, sources);

  if (!json.HasMember("y_array") || !json["y_array"].IsObject())
  {
    throw std::runtime_error(
      this->ObjectName  + " must provide a y_array object.");
  }
  this->YArray.reset(new Array());
  const auto& yarray = json["y_array"];
  this->YArray->ProcessJSON(yarray, sources);

  if (!json.HasMember("z_array") || !json["z_array"].IsObject())
  {
    throw std::runtime_error(
      this->ObjectName  + " must provide a z_array object.");
  }
  this->ZArray.reset(new Array());
  const auto& zarray = json["z_array"];
  this->ZArray->ProcessJSON(zarray, sources);

}

std::vector<vtkm::cont::VariantArrayHandle>
  ArrayCartesianProduct::Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const adis::metadata::MetaData& selections)
{
  std::vector<vtkm::cont::VariantArrayHandle> retVal;
  std::vector<vtkm::cont::VariantArrayHandle> xarrays =
    this->XArray->Read(paths, sources, selections);
  std::vector<vtkm::cont::VariantArrayHandle> yarrays =
    this->YArray->Read(paths, sources, selections);
  std::vector<vtkm::cont::VariantArrayHandle> zarrays =
    this->ZArray->Read(paths, sources, selections);
  size_t nArrays = xarrays.size();
  for(size_t i=0; i<nArrays; i++)
  {
    auto& xarray = xarrays[i];
    auto& yarray = yarrays[i];
    auto& zarray = zarrays[i];
    using floatType = vtkm::cont::ArrayHandle<float>;
    using doubleType = vtkm::cont::ArrayHandle<double>;
    if (xarray.IsType<floatType>() &&
        yarray.IsType<floatType>() &&
        zarray.IsType<floatType>())
    {
      auto xarrayF = xarray.Cast<floatType>();
      auto yarrayF = yarray.Cast<floatType>();
      auto zarrayF = zarray.Cast<floatType>();
      retVal.push_back(
        vtkm::cont::make_ArrayHandleCartesianProduct(
          xarrayF, yarrayF, zarrayF));
    }
    else if (xarray.IsType<doubleType>() &&
             yarray.IsType<doubleType>() &&
             zarray.IsType<doubleType>())
    {
      auto xarrayD = xarray.Cast<doubleType>();
      auto yarrayD = yarray.Cast<doubleType>();
      auto zarrayD = zarray.Cast<doubleType>();
      retVal.push_back(
        vtkm::cont::make_ArrayHandleCartesianProduct(
          xarrayD, yarrayD, zarrayD));
    }
    else
    {
      throw std::runtime_error(
        "Only float and double arrays are supported in cartesian products.");
    }
  }
  return retVal;
}

size_t ArrayCartesianProduct::GetNumberOfBlocks(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources)
{
  return this->XArray->GetNumberOfBlocks(paths, sources);
}
}
}
